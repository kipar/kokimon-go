require "./libnonoengine.cr"
require "./scales"

TUTORIALS = Set(String).new

enum Phase
  Change
  Glimpse
  None
end

PHASE_TIME   = 120
GLIMPSE_TIME =   4

class Dialog
  property next : Dialog?

  MAX_WIDTH = 60
  @glimpse : Int32

  def initialize(@game : Game, @who : Int32, @text : Array(String), @first_list = [] of Int32, @second_list = [] of Int32, @phase_effect = Phase::None, @phase_to = 0, @locked = false)
    @counter = 0
    @glimpse = rand(100) + 100
  end

  def draw
    Engine.layer = LAYER_GUI + 2
    # portraits
    # @my_face.show
    panel(20, 20, 6*48, 8*48, halign: HAlign::Left) do
      @counter += 1
      case @phase_effect
      when .glimpse?
        if (@counter - @glimpse).abs < GLIMPSE_TIME
          RES::Tiles::Portraits.draw_frame(@phase_to, *GUI.center, kx: 6, ky: 8)
        else
          RES::Tiles::Portraits.draw_frame(@who, *GUI.center, kx: 6, ky: 8)
        end
      when .change?
        alpha = {@counter * 255 // PHASE_TIME, 255}.min
        RES::Tiles::Portraits.draw_frame(@who, *GUI.center, kx: 6, ky: 8, color: color(255, 255, 255, 255 - alpha))
        RES::Tiles::Portraits.draw_frame(@phase_to, *GUI.center, kx: 6, ky: 8, color: color(255, 255, 255, alpha))
      else
        RES::Tiles::Portraits.draw_frame(@who, *GUI.center, kx: 6, ky: 8)
      end
    end

    panel(100, 100, 800, 300, HAlign::Center, VAlign::Center, fill: color(0x001000ff)) do
      @text.each_with_index do |s, i|
        if s == "L1" || s == "L2"
          list = s == "L1" ? @first_list : @second_list
          panel(50, 5, 800, 70, HAlign::Left, VAlign::Flow) do
            list.each do |u|
              panel(5, 5, 64, 64, halign: HAlign::Flow, fill: color(0xf0f060ff)) do
                RES::Tiles::Units.draw_frame(u, *GUI.center)
              end
            end
          end
        else
          if s.size > MAX_WIDTH
            list = [] of String
            line = ""
            s.split(' ').each do |tok|
              if tok.size + line.size < MAX_WIDTH
                line = (line == "") ? tok : (line + " " + tok)
              else
                list << line
                line = tok
              end
            end
            list << line unless line == ""
            list.each do |line|
              label(line, FONTS[UseFont::Usual], 10, 5, 800, 30, HAlign::Left, VAlign::Flow)
            end
          else
            label(s, FONTS[UseFont::Usual], 10, 5, 800, 30, HAlign::Left, VAlign::Flow)
          end
        end
      end
      label("Нажмите Enter Для продолжения", FONTS[UseFont::Usual], 10, 5, 750, 30, HAlign::Left, VAlign::Bottom, text_halign: HAlign::Right) unless @locked
    end
  end

  def process
    @game.dialog = @next if Keys[Key::Return].pressed? && !@locked
  end
end

def show_message(game, portrait, text, first_list = [] of Int32, second_list = [] of Int32, phase_effect = Phase::None, phase_to = 0, locked = false)
  dia = Dialog.new(game, portrait, text.split('|'), first_list, second_list, phase_effect, phase_to, locked)
  prev = game.dialog
  if prev
    loop do
      x = prev.next
      break if x.nil?
      prev = x
    end
    prev.next = dia
  else
    game.dialog = dia
  end
end

def show_tutorial(key, game, portrait, text, first_list = [] of Int32, second_list = [] of Int32, phase_effect = Phase::None, phase_to = 0, locked = false)
  return if TUTORIALS.includes?(key)
  TUTORIALS << key
  show_message(game, portrait, text, first_list, second_list, phase_effect, phase_to, locked)
end
