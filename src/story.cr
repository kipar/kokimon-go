require "./dialog_screen"

def initial_story(game)
  show_message(game, 48, "Наш план по захвату мира готов к исполнению, осталась единственная проблема.")
  show_message(game, 53, "Что, та маленькая девочка которая по пророчеству нас остановит?")
  show_message(game, 48, "Она самая.")
  show_message(game, 53, "Вы что, не можете послать к ней убийц или бросить ее в темницу?")
  show_message(game, 48, "Я видела множество фильмов с подобным сюжетом.|Главный герой всегда выбирается из невероятных испытаний и побеждает.")
  show_message(game, 53, "Тогда попробуем ее задержать. На несколько тысяч лет или около того.|Отправим в подземелье где выход есть только наверху,| а все лестницы ведут вниз.")
  show_message(game, 48, "А, то самое где нет ничего интересного кроме жуков и пауков?|Хм, может сработать.")
  show_message(game, 53, "(творит магию)", phase_effect: Phase::Change, phase_to: 54)
  show_message(game, 48, "Отлично. Теперь к деловой части. |Первая часть плана включает в себя выпуск нового интерфейса на ipad и iphone|Прогноз цены акций Apple составит в первом квартале...")
end

def game_won(game)
  # 15
  show_message(game, 15, "Еще один побитый враг...", phase_effect: Phase::Glimpse, phase_to: 37)
  show_message(game, 15, "Хм. Странное ощущение...", phase_effect: Phase::Glimpse, phase_to: 37)
  show_message(game, 15, "Кровавый ритуал завершен", phase_effect: Phase::Change, phase_to: 37)
  army = (game.player.inventory + game.player.equip.select { |x| x }.map(&.not_nil!)).sort_by { |u| {-u.level, u.typ} }.map(&.tile)
  show_message(game, 37, "Эти неудачники хотели избавиться от меня, но лишь помогли мне пробудиться.|Активирую телепорт и буду смотреть как мои собранные зверьки растерзают их|L1", army)
  show_message(game, 37, "(творит магию)", phase_effect: Phase::Change, phase_to: 15)
  show_message(game, 15, "Что со мной было? Я потеряла сознание?|А что это за портал?")
  show_message(game, 15, "КОНЕЦ ПЕРВОЙ ЧАСТИ.|L1", army, locked: true)
end
