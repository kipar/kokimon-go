require "./libnonoengine.cr"
require "./resources.cr"
require "./scales.cr"
require "./tiled_ground.cr"
require "./map.cr"
require "./unitsgen.cr"
require "./story.cr"

include Engine

Engine[Params::Antialias] = 4
Engine[Params::VSync] = 1
Engine[Params::Autoscale] = 1
Engine[Params::ClearColor] = 0
# Engine[Params::Fullscreen] = 1
# Engine[Params::Width] = 800
# Engine[Params::Height] = 600

LibEngine.init "resources"

FONTS[UseFont::Usual] = Font.new(RES::Consola)
FONTS[UseFont::Small] = Font.new(RES::Consola, char_size: 14)
FONTS[UseFont::SmallGreen] = Font.new(RES::Consola, char_size: 14, color: color(0x00ff00ff))
FONTS[UseFont::SmallRed] = Font.new(RES::Consola, char_size: 14, color: color(0xff0000ff))
FONTS[UseFont::Log] = Font.new(RES::Arial, char_size: 14)

# begin
monsters = generate_monsters

game = Game.new(monsters)
initial_story(game)
game.new_game
loop do
  process
  break if !Keys[Key::Quit].up?
  # Engine[Params::Fullscreen] = 1 - Engine[Params::Fullscreen] if Keys[Key::F12].pressed?

  game.draw
  game.process
end
# rescue ex
#   log(ex.inspect_with_backtrace)
# end
