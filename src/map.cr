require "./libnonoengine.cr"
require "./scales"
require "./mapgen"
require "./battle"
require "./battle_screen"
require "./unitsgen"
require "./inventory_screen"
require "./dialog_screen"

record Cell, passable : Bool, transparent : Bool, bigroom : Bool, tile : Int32 do
  def recolor(atile)
    Cell.new(@passable, @transparent, @bigroom, atile)
  end
end

alias MapData = Array(Cell)

FOREST     = Cell.new(false, false, false, 1)
GRASS      = Cell.new(true, true, false, 5)
GRASS_ROOM = Cell.new(true, true, true, 5)

MAX_BATTLE = 3

abstract class MapGenerator
  abstract def generate(sx, sy) : MapData
end

class DummyMapGenerator
  def generate(sx, sy, data : MapData)
    sy.times do |y|
      sx.times do |x|
        data[x + y*sx] = rand < 0.3 ? FOREST : GRASS
      end
    end
    sy.times do |y|
      data[0 + y*sx] = FOREST
      data[sx - 1 + y*sx] = FOREST
    end
    sx.times do |x|
      data[x + 0*sx] = FOREST
      data[x + (sy - 1)*sx] = FOREST
    end
  end
end

class DungeonMapGenerator
  def initialize
  end

  def generate(sx, sy, data : MapData)
    raise "only square maps supported" unless sx == sy
    raw = BearMG.generate_map({sx, sy}.min, BearLibMG::MapGenerator::G_NICE_DUNGEON, 0)
    sy.times do |y|
      sx.times do |x|
        data[x + y*sx] = case raw[x + y*sx]
                         when BearLibMG::CellType::TILE_CORRIDOR
                           GRASS
                         when BearLibMG::CellType::TILE_SMALLROOM
                           GRASS
                         when BearLibMG::CellType::TILE_BIGROOM
                           GRASS_ROOM
                         when BearLibMG::CellType::TILE_DOOR
                           GRASS
                         else
                           FOREST
                         end
      end
    end
    sy.times do |y|
      data[0 + y*sx] = FOREST
      data[sx - 1 + y*sx] = FOREST
    end
    sx.times do |x|
      data[x + 0*sx] = FOREST
      data[x + (sy - 1)*sx] = FOREST
    end
  end
end

enum AnimationFrame
  Walk1 = 0
  Stand = 1
  Walk2 = 2
end

def dist(x1, y1, x2, y2)
  Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2))
end

class Player
  property x = 0
  property y = 0
  property level = 0
  property map : Map
  property army = [] of SomeUnit
  property inventory = [] of SomeUnit
  property equip = [] of SomeUnit?
  property sleeping = 0
  @moved_dx = 0
  @moved_dy = 1
  @started_move = Time.monotonic
  @last_moved = Time.monotonic
  property base_frame : Int32
  property face : Int32
  @revert_x_frame = false
  getter speed : Int32

  def initialize(@map, @x, @y, @speed, level, @base_frame, @face)
    @equip = [] of SomeUnit?
  end

  def generate_army(monsters, level, good)
    base_list = monsters.select { |x| x.level == level }
    m1 = base_list.sample
    m3 = base_list.select { |x| x.typ == m1.typ }.sample
    if good
      new_list = monsters.select { |x| (x.level == level + 1) && x.typ == m1.typ }
      new_list = monsters.select { |x| x.level == level + 1 } if new_list.size == 0
      m2 = new_list.sample
    else
      case rand(3)
      when 0
        m2 = base_list.select { |x| x.typ == m1.typ }.sample
      when 1
        m2 = base_list.sample
      when 2
        new_list = monsters.select { |x| (x.level - level).abs == 1 && x.typ == m1.typ }
        if new_list.size == 0
          m2 = base_list.sample
        else
          m2 = new_list.sample
        end
      end
    end
    @equip << m1.clone
    @equip << m2.clone
    @equip << m3.clone
  end

  def fill_army
    @army.clear
    MAX_BATTLE.times do |i|
      if @equip[i] == nil
        break if @inventory.size == 0
        max_level = @inventory.max_of(&.level)
        x = @inventory.select { |u| u.level == max_level }.sample
        @equip[i] = x
        @inventory.delete(x)
      end
    end
    @army = @equip.select { |x| x }.map(&.not_nil!.clone)
    # p "filling army: #{@equip.map(&.not_nil!.tile)} => #{@army.map(&.tile)}"
  end

  def spend_army
    @equip.fill(nil)
    @army.each_with_index do |u, i|
      if u.dead
        @equip[i] = nil
      else
        @equip[i] = u.parent
      end
    end
    @army.clear
  end

  def motion_frame(n)
    # going up
    @revert_x_frame = false
    if @moved_dx == 0
      if @moved_dy < 0
        return 0 + n
        # going down
      else # if @moved_dy > 0
        return 20 + n
      end
    else
      @revert_x_frame = @moved_dx < 0
      return 10 + n
    end
  end

  def moving
    return (Time.monotonic - @last_moved).total_milliseconds < WALK_ANIM_STOP
  end

  def draw
    if moving
      ms = (Time.monotonic - @started_move).total_milliseconds
      frame = ((ms // (WALK_ANIM_SPEED + 1)) % 3) # == 0 ? AnimationFrame::Walk1.to_i : AnimationFrame::Walk2.to_i
    else
      frame = AnimationFrame::Stand.to_i
    end
    frame = motion_frame(frame)

    RES::Tiles::Characters.draw_frame(@base_frame + frame, x - TILE_SIZE // 2, y - TILE_SIZE*9//10, kx: PLAYER_SCALE * (@revert_x_frame ? -1 : 1), ky: PLAYER_SCALE)
  end

  def step(dx, dy)
    # newpos = {(@x+dx) / }
    newx, newy = @x + dx*@speed, @y + dy*@speed
    @moved_dx = dx
    @moved_dy = dy
    if !moving
      @started_move = Time.monotonic
    end
    @last_moved = Time.monotonic
    if !@map.cell(newx//TILE_SIZE, newy//TILE_SIZE).passable
      if @map.cell(@x//TILE_SIZE, newy//TILE_SIZE).passable
        newx = @x
      elsif @map.cell(newx//TILE_SIZE, @y//TILE_SIZE).passable
        newy = @y
      else
        return false
      end
    end
    moved = newx != @x || newy != @y
    @x, @y = newx, newy
    return moved
  end

  @walk_x = -1
  @walk_y = -1

  DEBUG_BATTLE = 1.0

  def ai(target)
    if @sleeping > 0
      @sleeping -= 1
      return false
    end
    if dist(@x, @y, target.x, target.y) < 3*DEBUG_BATTLE*TILE_SIZE
      auto_step(target.x, target.y)
      return true if dist(@x, @y, target.x, target.y) < DEBUG_BATTLE*TILE_SIZE/2
    else
      if rand < 0.1 || @walk_x < 0
        @walk_x, @walk_y = @map.freepoint_near(@x, @y)
      end
      @walk_x = -1 if !auto_step(@walk_x, @walk_y)
      # step(rand(3) - 1, rand(3) - 1)
    end
    false
  end

  def auto_step(tx, ty)
    dx = (tx - @x)
    dx = 0 if dx.abs < 10
    dy = (ty - @y)
    dy = 0 if dy.abs < 10
    step(dx.sign, dy.sign)
  end
end

class Map
  getter size_x : Int32
  getter size_y : Int32
  @data : MapData
  getter wall : Int32
  getter floor : Int32
  getter checked_points : { {Int32, Int32}, {Int32, Int32} }

  def cell(x, y)
    @data[x + size_x*y]
  end

  def each_cell(&)
    size_y.times do |y|
      size_x.times do |x|
        yield(cell(x, y), x, y)
      end
    end
  end

  def freepoint_near(x, y)
    ax = x // TILE_SIZE
    ay = y // TILE_SIZE
    cx = 0
    cy = 0
    10000.times do
      cx = ax - 10 + rand(20)
      cy = ay - 10 + rand(20)
      break if cx >= 0 && cx < @size_x && cy >= 0 && cy < @size_y && cell(cx, cy).passable
    end
    cx = cx * TILE_SIZE + TILE_SIZE // 2
    cy = cy * TILE_SIZE + TILE_SIZE // 2
    {cx, cy}
  end

  def freepoint(scaled = true, only_room = false)
    ax = size_x // 2
    ay = size_y // 2
    10000.times do
      ax = rand(size_x)
      ay = rand(size_y)
      if only_room
        break if cell(ax, ay).bigroom
      else
        break if cell(ax, ay).passable
      end
    end
    if only_room
      return {0, 0} if (!cell(ax, ay).bigroom)
    else
      return {0, 0} if (!cell(ax, ay).passable)
    end

    if scaled
      ax = ax*TILE_SIZE + TILE_SIZE // 2
      ay = ay*TILE_SIZE + TILE_SIZE // 2
    end
    return {ax, ay}
  end

  def check_connected(x1, y1, x2, y2)
    wave_data = Array(Int32).new(@size_x*@size_y, 0)
    (@size_x * @size_y).times do |i|
      pass = @data[i].passable
      wave_data[i] = pass ? 0 : -1
    end
    wave_data[y1 * @size_x + x1] = 1
    # wave algorithm
    iter = 1
    loop do
      changed = false
      (@size_x * @size_y).times do |i|
        next unless wave_data[i] == iter
        x = i % @size_x
        y = i // @size_x
        # paint neighbours
        { {0, 1}, {0, -1}, {1, 0}, {-1, 0} }.each do |(dx, dy)|
          ny = y + dy
          next if ny < 0 || ny >= @size_y
          nx = x + dx
          next if nx < 0 || nx >= @size_x
          ni = ny * @size_x + nx
          next if wave_data[ni] != 0
          changed = true
          wave_data[ni] = iter + 1
        end
      end
      break unless changed
      iter += 1
    end
    # (@size * @size).times do |i|
    #   return false if @wave_data[i] == 0
    # end
    wave_data[y1 * @size_x + x1] != 0
  end

  def initialize(sx, sy, generator, @floor, @wall)
    @size_x = sx
    @size_y = sy
    @checked_points = { {0, 0}, {1, 1} }
    @data = MapData.new(sx*sy, FOREST)
    guard = 1
    loop do
      guard += 1
      generator.generate(sx, sy, @data)
      10.times do
        @checked_points = {freepoint(false, true), freepoint(false, true)}
        next if dist(*checked_points[0], *checked_points[1]) > DEMO_SIZE/4
      end
      break if check_connected(*checked_points[0], *checked_points[1])
      raise "failed to generate map" if guard > 1000
    end
  end

  def draw(px, py)
    # each_cell do |c, x, y|
    #   RES::Tiles::Ground.draw_frame(c.tile, x*TILE_SIZE, y*TILE_SIZE, kx: TILE_SCALE, ky: TILE_SCALE)
    # end

    show_ground(size_x, size_y, @floor, RES::Tiles::Ground, 51, px, py) do |x, y|
      true
    end
    show_ground(size_x, size_y, @wall, RES::Tiles::Ground, 51, px, py) do |x, y|
      !cell(x, y).transparent
    end
  end
end

class Game
  getter map
  getter player
  property cur_level = 0
  property battle : Battle?
  property battler : Player?
  getter! inventory : InventoryWindow
  property dialog : Dialog?
  property monster_db = [] of SomeUnit

  property list_captured = [] of SomeUnit
  property list_lost = [] of SomeUnit

  getter monsters = [] of Player
  property loot = [] of Steppable

  property killed_top_level = 0
  property fighting_top_level = false

  def initialize(@monster_db)
    @map = Map.new(DEMO_SIZE, DEMO_SIZE, DummyMapGenerator.new, WALL_TILES.sample, FLOOR_TILES.sample)
    @player = Player.new(@map, 0, 0, PLAYER_SPEED, -1, 30*PLAYER_TILE, PLAYER_FACE)

    @inventory = nil
    @inventory = InventoryWindow.new(self)
  end

  def new_game
    @player.equip.clear
    @player.generate_army(@monster_db, 0, true)
    @player.inventory = @player.equip.select { |x| x }.map(&.not_nil!)
    @player.equip.clear
    @player.generate_army(@monster_db, 0, true)
    @inventory = nil
    @inventory = InventoryWindow.new(self)
    @cur_level = 0
    @battle = nil
    show_message(self, @player.face, "Подземелье с крысами, жуками и пауками??|К такому жизнь меня не готовила. Попробую найти союзников среди местных зверушек.")
    show_tutorial("controls", self, @player.face, "Управление:|   стрелки/WSAD - движение|   пробел - ждать|   I - инвентарь")
    next_level
  end

  def next_level
    @should_go_down = false
    @cur_level += 1
    old_equip = @player.equip
    old_inventory = @player.inventory
    gx, gy = 0, 0
    @monsters.clear
    @map = Map.new(DEMO_SIZE, DEMO_SIZE, DungeonMapGenerator.new, FLOOR_TILES.sample, WALL_TILES.sample)
    gx, gy = @map.checked_points[0]
    gx = gx*TILE_SIZE + TILE_SIZE // 2
    gy = gy*TILE_SIZE + TILE_SIZE // 2
    @player = Player.new(@map, gx, gy, PLAYER_SPEED, -1, 30*PLAYER_TILE, PLAYER_FACE)
    @player.equip = old_equip
    @player.inventory = old_inventory
    base_lvl = (@cur_level*DIFFICULTY_RATE).to_i
    (MONSTERS_COUNT).times do
      gx, gy = @map.freepoint
      next if dist(gx, gy, @player.x, @player.y) < 10*TILE_SIZE
      m_lvl = base_lvl
      m_lvl += 1 if rand < 0.5
      m_lvl -= 1 if rand < 0.1
      m_lvl = 0 if m_lvl < 0
      m_lvl = 4 if m_lvl > 4
      m = Player.new(@map, gx, gy, ENEMY_SPEED, m_lvl, 30*ENEMY_TILES[m_lvl], ENEMY_FACES[m_lvl])
      m.generate_army(@monster_db, m_lvl, false)
      @monsters << m
    end
    @loot.clear
    gx, gy = @map.checked_points[1]
    gx = gx*TILE_SIZE + TILE_SIZE // 2
    gy = gy*TILE_SIZE + TILE_SIZE // 2
    @loot << Stair.new(self, gx, gy)
    (STAIRS_COUNT - 1).times do
      100.times do
        gx, gy = @map.freepoint(only_room: true)
        break if (gx - @player.x) > 10*TILE_SIZE || (gy - @player.y) > 10*TILE_SIZE
      end
      @loot << Stair.new(self, gx, gy)
    end
    (LOOT_COUNT).times do
      gx, gy = @map.freepoint
      next if (gx - @player.x) < 2*TILE_SIZE && (gy - @player.y) < 2*TILE_SIZE
      @loot << Present.new(self, gx, gy, {(@cur_level*DIFFICULTY_RATE).to_i + 1, 4}.min)
    end

    GC.collect
  end

  def draw
    camera(@player.x - Engine[Params::Width] // 2, @player.y - Engine[Params::Height] // 2)
    @map.draw(@player.x // TILE_SIZE, @player.y // TILE_SIZE)
    @loot.each &.draw
    @monsters.each &.draw
    @player.draw

    if d = @dialog
      d.draw
    end
    if inventory.visible
      inventory.draw
    end
    if b = @battle
      b.draw
    end
  end

  def do_step(dx, dy)
    return unless @player.step(dx, dy)
    process_monsters
    process_loot
  end

  def process_monsters
    @monsters.each do |u|
      if u.ai(@player)
        start_battle(u)
        break
      end
    end
  end

  property should_go_down = false

  def process_loot
    @loot.reject! do |it|
      near = dist(it.x, it.y, @player.x, @player.y) < TILE_SIZE/2
      it.on_step(@player) if near
      near
    end
    next_level if @should_go_down
  end

  def start_battle(who)
    @list_captured.clear
    @list_lost.clear
    @battler = who
    @player.fill_army
    show_tutorial("battle", self, @player.face, "Похоже не одна я собираю зверушек.|Зверушки, в атаку!|#{@player.army.sample.name.capitalize}, куси их!")
    who.fill_army
    @fighting_top_level = who.army.any? { |x| x.level >= 4 }

    if @player.army.size == 0
      game_lost
    else
      @battle = Battle.new(self, {@player.army, who.army}, {@player.inventory, who.inventory}, {@player.face, who.face})
    end
  end

  def end_battle(player_won)
    return unless who = @battler
    if @player.army.size == 0
      game_lost
      return
    end
    if player_won
      s = ""
      s += "|Захваченные зверушки:|L1" if @list_captured.size > 0
      s += "|Зверушки которыe дальше драться не будут:|L2" if @list_lost.size > 0
      l1 = @list_captured.map(&.tile)
      l2 = @list_lost.map(&.tile)
      if TUTORIALS.includes?("battle_won")
        show_message(self, @player.face, "Бой окончен." + s, l1, l2, phase_effect: Phase::Glimpse, phase_to: 37) if s != ""
      else
        show_tutorial("battle_won", self, @player.face, "Заходи слева, шпыняй его лапой!|А... уже всё закончилось." + s, l1, l2)
      end
      @monsters.delete(who)

      if @fighting_top_level
        @killed_top_level += 1
        game_won(self) if @killed_top_level >= 5
      end
    else
      who.spend_army
      who.sleeping = 10
    end

    @player.spend_army
    @battle = nil
    @battler = nil
    GC.collect
  end

  def game_lost
    show_message(self, @player.face, "Уф, чудом смогла убежать. Ну что же, придется начать всё сначала", phase_effect: Phase::Glimpse, phase_to: 37)
    new_game
  end

  def process
    if d = dialog
      d.process
      return
    end
    if inventory.visible
      inventory.process
      return
    end
    if b = @battle
      b.process
      return
    end
    if !Keys[Key::I].up?
      inventory.show
      return
    end

    RES::Music.music
    dx = 0
    dy = 0
    # next_level if Keys[Key::F1].pressed?
    dy -= 1 if !Keys[Key::W].up? || !Keys[Key::Up].up?
    dy += 1 if !Keys[Key::S].up? || !Keys[Key::Down].up?
    dx -= 1 if !Keys[Key::A].up? || !Keys[Key::Left].up?
    dx += 1 if !Keys[Key::D].up? || !Keys[Key::Right].up?
    do_step(dx, dy) unless dx == 0 && dy == 0
    process_monsters if !Keys[Key::Space].up?
    # game_won(self) if !Keys[Key::F2].up?
  end
end

abstract class Steppable
  property x = 0
  property y = 0

  def initialize(@game : Game, @x, @y)
  end

  abstract def draw
  abstract def on_step(player)
end

class Stair < Steppable
  def on_step(player)
    # show_tutorial("go_down", @game, player.face, "Ура! Лестница к еще более интересным зверушкам", phase_effect: Phase::Glimpse, phase_to: 37)
    show_message(@game, player.face, "Ура! Лестница к еще более интересным зверушкам", phase_effect: Phase::Glimpse, phase_to: 37)
    @game.should_go_down = true
  end

  def draw
    RES::Stair.draw(x - TILE_SIZE//2, y - TILE_SIZE//2)
    # RES::Tiles::Present.draw_frame(0, x - 50, y - 50)
    # RES::Tiles::Present.draw_frame(1, x , y - 50)
    # RES::Tiles::Present.draw_frame(2, x + 50, y - 50)
    # RES::Tiles::Present.draw_frame(3, x + 100, y - 50)
  end
end

class Present < Steppable
  def initialize(game, x, y, @level : Int32)
    super(game, x, y)
  end

  def on_step(player)
    item = @game.monster_db.select { |x| x.level == @level }.sample
    show_tutorial("take_item", @game, player.face, "L1|Ух ты!|Зверушка в красивой упаковке, еще и перевязанная ленточкой!||Нажмите I чтобы посмотреть инвентарь", [item.tile])
    player.inventory << item.clone
  end

  def draw
    tile = @level - 1
    tile = 0 if tile < 0
    tile = 3 if tile > 3
    RES::Tiles::Present.draw_frame(tile, x - TILE_SIZE//2, y - TILE_SIZE//2)
  end
end
